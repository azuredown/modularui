using UnityEngine;

/// <summary>
/// A script that holds all the locations of objects in this scene.
/// </summary>
public static class SceneLocations {

	/// <summary>
	/// Normally I'd just have a camera script attached to the main camera to do this but this is OK too I guess.
	/// </summary>
	private static Camera _mainCamera;
	public static Camera mainCamera {
		get {
			if (_mainCamera == null) {
				_mainCamera = Camera.main;
			}

			return _mainCamera;
		}
	}

	public static UpdateRoot updateRoot;
}