This repository contains 2 things: the ModularUI system and the testing framework that allows inserting of mock input. The two projects originally were in sepparate repositories but it's a little simpler to have them both in the same repository so I merged them.

# ModularUI

Conventional UI in Unity involves all UI elements recieving an `Update()` call like so:

![Normal UI](Images/Normal UI.png)

Each UI element then does whatever it needs to do to process UI input.

This system has a whole host of problems. The most obvious of which is how do you know if one UI object is occluded (covered) by another. This could be solved by raycasting but then you have to consider clicks of newly interactable UI. Then you have to figure out which UI element the user intended to interact with. This may be simple if you're just clicking, but many games lets the user drag.

And finally there's the problem of letting the user drag in two panes at the same time (don't judge me).

My answer to these problems was to have multiple hierarchies of UI elements. Overtime this system grew to encompass all UI (everything you can interact with really) and has been significantly simplified. This system I call ModularUI and it looks like this:

![ModularUI](Images/ModularUI.png)

# Getting Started

1. Copy the following files/directories into your project directory:

- ModularCore/ (this holds the main ModularUI components)
- BasicTypes/
- SceneLocations.cs

2. Next make each of your UI elements inherit from `ModularUI` (inside ModularCore). This will require you to implement the `updateThis()` method. Think of this as Unity's `Update()` method except it passes in a bunch of useful information. Oh, a list of InputVectors doesn't seem useful at first but consider the methods `getClickingVector()`, `getDraggingVector()`, and `clicking()`. Each one of these uses the power of ModularUI to only listen to the input vectors we care about.
3. Make each `ModularUI` automatically add its children before `Update()` is called using the `addElement()` method. This should be done in `Awake()` or `Start()`. For example the way I do this is:

 ```csharp
private void Start() {
	addElement(SceneLocations.menuButton);
	addElement(SceneLocations.cycleUnits);
	addElement(SceneLocations.zoomButton);
	addElement(SceneLocations.topPane);
	addElement(SceneLocations.bottomPane);
}
```

Note that the SceneLocations.cs file I use has more stuff in it each initialized in `Awake()`.

4. Place the `UpdateRoot` script on a prefab and include it in every scene.
5. Populate `UpdateRoot.rootModularUI` (a serialized field) with your root `ModularUI`. This is the beginning of the ModularUI hierarchy. Below is an image of my ModularUI hierarchy (in the game view, during the level loader I completely swap out everything under `InputController` in order to radically change behaviour):

![My ModularUI Hierarchy](Images/My ModularUI Hierarchy.png)

# Features

## InputVector

InputVector is perhaps the most important functionality of ModularUI (at least on devices with touchscreens). Think of an InputVector as a wrapper for mouse and touch inputs. Because it is essentially that with a few more integrations with the rest of ModularUI.

Although on non-touchscreen devices it still has it's uses. It can be used to identify long presses, double presses, and holds the time the player spent pressing. It can also be used, in conjunction with other classes not included, to check for gestures such as dragging and pinching.

## Blocking UI

There may be times that you want a full screen UI window to block the entire game. In these cases you can use blocking UI. Adding blocking UI is done by calling `addBlockingLeafNode()` and it can be removed by calling `removeBlockingNode()`. As it's name implies blocking UI blocks all other UI (that is not a child of itself) from getting input.

Think of this like a branch of the tree which. All the input usually flows to some branch based on logic, but if a blocking UI branch is present all input gets redirected to it. This also applies to things like keypresses.

## Keyboard Input

TODO

## Profiling

Because ModularUI manages all the input calls we can also add profiling for free... or very cheaply at least. Open the profiler and you can clearly see the modular UI tree of `update()` calls. This also uses the parameter `profilerName` to avoid generating garbage from string concatenation.

## Mock Input

Because ModularUI uses a wrapper around inputs we can easily add 'mock' inputs for testing. To do so simply write `InputVector.addInputVector(new InputSource(InputType.Test), new List<ModularUI>{ button });` in your code where button is the ModularUI element you wish to interact with.

Then you have to configure the location and state of your mock input. To do so find all the lines that say "// Replace these two lines to get test input working" and edit the following two lines to take input from your testing class rather than use the default values. In my code I simply use the properties `Tester.touchLocation` and `Tester.inputState`.

To have the tests working properly the input states must cycle through the states `InputState.Begin`, `InputState.Interacting`, `InputState.End`, and `InputState.Deleted` and each must be active for one frame. Ideally this would be done in a coroutine. Note that if you do not use the state `InputState.Deleted` your mock input will never be deleted.

# Unity Test Framework

A framework that allows for easy testing from within the game.

## Getting Started

This framework relies on `Tester.initializeTests()` being called to run tests. The recommended way to do that is to use the IngameDebugConsole. It has its own README which explains how to use it. Alternately you can use your own custom solution to call `initializeTests()`. If you do this be sure to remove all references of the `IngameDebugConsole` from `Tester`.

Next `Tester.update()` must be called every frame. An easy way to do this is have some other `MonoBehaviour` call it from its own `Update`. Alternately you can just convert `Tester` into a `MonoBehaviour` and convert `update()` to `Update()`.

Next you will need to write your tests. A test is just a coroutine (an `IEnumerator` object) that throws an exception upon reaching an error.

Next enqueue all your tests in `Tester.initializeTests()` by hard coding them before the line `totalTests = tests.Count`. Each line should look something like this.

`tests.Enqueue(TestFAB.TestFABClicking(counter));`

Finally to run your tests run `initializeTests()` (if you're using IngameDebugConsole this is done by typing in the command 'RunTests') The results of the test will be written to the console.
