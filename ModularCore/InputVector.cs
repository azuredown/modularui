﻿using System;
using System.Collections.Generic;
using IngameDebugConsole;
using JetBrains.Annotations;
using UnityEngine;

namespace ModularCore {

	public enum InputType {
		Mouse,
		Touch,
		Test,
	}

	public struct InputSource {
		public readonly InputType type;
		public readonly int touchID;

		public InputSource(InputType type) {
			if (type == InputType.Touch) {
				Debug.LogError("Not passing touchID!");
			}
			this.type = type;
			this.touchID = 0;
		}

		public InputSource(int touchID) {
			this.type = InputType.Touch;
			this.touchID = touchID;
		}
	}

	public class InputVector {

		public const float longPressTime = 0.5f;
		private const float minTouchDistanceToResetTimerWRTScreen = 1f / 64;
		private static float maxDistanceCanBeConsideredTap => minTouchDistanceToResetTimerWRTScreen * Mathf.Max(Screen.height, Screen.width);

		private static ListDictionary<Vector2, float> doubleTapHolder = new ListDictionary<Vector2, float>();

		private InputSource inputSource;

		public float timeSpentPressing;
		public bool doubleTap = false;
		private bool inputStart { get; set; }
		// Stores if this input moves too much to be considered a click.
		private bool stationaryInput = true;
		public InputState state {
			get {
				switch (inputSource.type) {
					case InputType.Mouse:
						if (Input.GetMouseButtonUp(0)) {
							return InputState.End;
						} else if (Input.GetMouseButtonDown(0)) {
							return InputState.Begin;
						} else if (Input.GetMouseButton(0)) {
							return InputState.Interacting;
						} else {
							return InputState.Deleted;
						}
					case InputType.Touch:
						Touch? touch = getTouchByID(inputSource.touchID);

						if (touch == null) {
							return InputState.Deleted;
						}

						switch (touch.Value.phase) {
							case TouchPhase.Began:
								return InputState.Begin;
							case TouchPhase.Moved:
							case TouchPhase.Stationary:
								return InputState.Interacting;
							case TouchPhase.Ended:
							case TouchPhase.Canceled:
								return InputState.End;
							default:
								Debug.LogError("Unknown state: " + touch.Value.phase);
								return InputState.Interacting;
						}
					case InputType.Test:
						return Tester.inputState;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public enum InputState {
			Begin,
			End,
			Interacting,
			Deleted, // If this input ID no longer exists
		}

		public bool longTouch => timeSpentPressing >= longPressTime;

		private Vector2 initialScreenSpacePosition;
		/// <summary>
		/// From the time we begin clicking and the time we end clicking it is possible for the camera to move slightly.
		/// This variable allows us to remember what we started clicking on.
		/// </summary>
		public Vector2 initialWorldSpacePosition;
		public Vector2 lastKnownScreenSpacePosition;

		public bool clicking => state == InputState.End && !longTouch &&
								Vector2.Distance(initialScreenSpacePosition, lastKnownScreenSpacePosition) <
								maxDistanceCanBeConsideredTap;

		public bool longClicking => state == InputState.End && longTouch &&
									Vector2.Distance(initialScreenSpacePosition, lastKnownScreenSpacePosition) <
									maxDistanceCanBeConsideredTap;

		/// <summary>
		/// A click at the end of a long drag. Ignores the distance check from normal click check.
		/// </summary>
		public bool dragClick => state == InputState.End && timeSpentPressing >= 0.25f;

		/// <summary>
		/// The modular UI or UI's we originally interacted with.
		/// This is a list rather than a single ModularUI so we can inform the parent if we got an input or not so the
		/// close window logic will work correctly in cases where we want to put a ModularUI on top of other ModularUI's.
		/// </summary>
		public List<ModularUI> inputOwners;

		private static readonly List<InputVector> inputVectorPool = new List<InputVector>();
		public static readonly List<InputVector> inputVectorsInUse = new List<InputVector>();

		/// <summary>
		/// ID is null for mouse and the finger id for touch.
		/// </summary>
		public static void addInputVector(InputSource inputSource, List<ModularUI> originalOwners = null) {
			InputVector finalVector;

			Vector2 initialPosition;

			if (DebugLogManager.instance.logWindowVisible && inputSource.type != InputType.Test) {
				return;
			}

			switch (inputSource.type) {
				case InputType.Mouse:
					initialPosition = Input.mousePosition;
					break;
				case InputType.Touch:
					Touch? touch = getTouchByID(inputSource.touchID);
					if (touch == null) {
						Debug.LogError("Touch not found.");
						initialPosition = Vector2.zero;
					} else {
						initialPosition = touch.Value.position;
					}
					break;
				case InputType.Test:
					initialPosition = Tester.touchLocation;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(inputSource.type));
			}

			if (originalOwners == null) {
				originalOwners = SceneLocations.inputController.findOriginalOwner(initialPosition);
			}

			if (inputVectorPool.Count == 0) {
				finalVector = new InputVector(inputSource, initialPosition, originalOwners);
			} else {
				finalVector = inputVectorPool[inputVectorPool.Count - 1];
				inputVectorPool.Remove(finalVector);
				finalVector.setID(inputSource, initialPosition, originalOwners);
			}

			inputVectorsInUse.Add(finalVector);
		}


		public static void debugDisableAllNonTestInputVectors() {
			for (int i = 0; i < inputVectorsInUse.Count; i++) {
				if (inputVectorsInUse[i].inputSource.type != InputType.Test) {
					inputVectorsInUse[i].repool();
				}
			}
		}


		private InputVector(InputSource inputSource, Vector2 initialScreenSpacePosition, List<ModularUI> originalOwners) {
			setID(inputSource, initialScreenSpacePosition, originalOwners);
		}


		private void setID(InputSource inputSource, Vector2 initialScreenSpacePosition, List<ModularUI> originalOwners) {
			this.inputSource = inputSource;
			this.initialScreenSpacePosition = initialScreenSpacePosition;
			lastKnownScreenSpacePosition = initialScreenSpacePosition;
			this.initialWorldSpacePosition = SceneLocations.mainCamera.ScreenToWorldPoint(initialScreenSpacePosition);
			timeSpentPressing = 0;
			inputStart = true;
			stationaryInput = true;
			doubleTap = false;
			this.inputOwners = originalOwners;

			// Check if is a double tap
			for (int i = 0; i < doubleTapHolder.count; i++) {
				if (Vector2.Distance(doubleTapHolder[i].item1, initialScreenSpacePosition) <=
					maxDistanceCanBeConsideredTap) {

					doubleTapHolder.removeAt(i);
					doubleTap = true;

					break;
				}
			}
		}


		[CanBeNull]
		private static Touch? getTouchByID(int id) {
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch(i).fingerId == id) {
					return Input.GetTouch(i);
				}
			}

			return null;
		}


		public static bool atLeastOneInputVectorClicking() {
			for (int i = 0; i < inputVectorsInUse.Count; i++) {
				if (inputVectorsInUse[i].clicking) {
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Maintains touch data. Should be called once a frame.
		/// </summary>
		public static void checkAllTouches() {
			for (int i = inputVectorsInUse.Count - 1; i >= 0; i--) {

				inputVectorsInUse[i].lastKnownScreenSpacePosition = getCurrentScreenSpacePosition(inputVectorsInUse[i].inputSource);
				inputVectorsInUse[i].timeSpentPressing += Time.deltaTime;

				// Clicking so fast that there isn't time to register a mouse up.
				if ((inputVectorsInUse[i].inputSource.type == InputType.Mouse && !(Input.GetMouseButtonDown(0) || Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))) ||
					(inputVectorsInUse[i].inputSource.type == InputType.Touch && !containsTouch(inputVectorsInUse[i].inputSource.touchID))) {

					// This tends to happen when there's a lag spike. Happens a lot in the editor but not in game.
#if !UNITY_EDITOR
					Debug.LogWarning("InputVector.checkAllTouches(): Mouse up event dropped. Type: " + inputVectorsInUse[i].inputSource.type);
#endif
					continue;
				}

				if (Vector2.Distance(inputVectorsInUse[i].lastKnownScreenSpacePosition,
						inputVectorsInUse[i].initialScreenSpacePosition) > maxDistanceCanBeConsideredTap) {
					inputVectorsInUse[i].stationaryInput = false;
				}
			}

			// Increment time for multitaps and expire them if they existed for too long
			for (int i = doubleTapHolder.count - 1; i >= 0; i--) {
				doubleTapHolder[i].item2 += Time.deltaTime;

				if (doubleTapHolder[i].item2 >= longPressTime) {
					doubleTapHolder.removeAt(i);
				}
			}
		}


		/// <summary>
		/// Called at the end of the frame to make sure long presses are taken into account properly and that
		/// touches that ended do not stick around for an extra frame
		/// </summary>
		public static void removeTouches() {
			for (int i = inputVectorsInUse.Count - 1; i >= 0; i--) {
				if (inputVectorsInUse[i].stationaryInput && inputVectorsInUse[i].timeSpentPressing >= longPressTime) {
					inputVectorsInUse[i].lastKnownScreenSpacePosition = getCurrentScreenSpacePosition(inputVectorsInUse[i].inputSource);
					if (inputVectorsInUse[i].doubleTap) {
						SceneLocations.inputController.displayDebugConsole();
					}
				}

				if (inputVectorsInUse[i].state == InputState.End || inputVectorsInUse[i].state == InputState.Deleted) {
					if (!inputVectorsInUse[i].doubleTap) {
						doubleTapHolder.add(inputVectorsInUse[i].lastKnownScreenSpacePosition, 0);
					}

					inputVectorsInUse[i].repool();
				}
			}
		}


		private static bool containsTouch(int touchID) {
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch(i).fingerId == touchID) {
					return true;
				}
			}

			return false;
		}


		private static Vector2 getCurrentScreenSpacePosition(InputSource inputSource) {
			switch (inputSource.type) {
				case InputType.Mouse:
					return Input.mousePosition;
				case InputType.Touch:
					return getTouchLocation(inputSource.touchID);
				case InputType.Test:
					return Tester.touchLocation;
				default:
					throw new ArgumentOutOfRangeException(nameof(inputSource.type), inputSource.type, null);
			}
		}
		private static Vector2 getTouchLocation(int touchID) {
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch(i).fingerId == touchID) {
					return Input.GetTouch(i).position;
				}
			}

			throw new Exception("Touch not found");
		}


		private void repool() {
			inputVectorsInUse.Remove(this);
			inputVectorPool.Add(this);
		}


		/*
		private static TouchPhase getTouchPhase(int touchID) {
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch(i).fingerId == touchID) {
					return Input.GetTouch(i).phase;
				}
			}

			throw new Exception("Touch not found");
		}

	public static bool longPress(byte bitString) {

		for (byte i = 0; i < Constants.maxSupportedTouches; i++) {
			if ((bitString & (1 << i)) != 0 && getInputVector(Input.GetTouch(i).fingerId).timeSpentPressing > longPressTime) {
				return true;
			}
		}
		if ((bitString & (1 << Constants.maxSupportedTouches)) != 0 && getInputVector(null).timeSpentPressing > longPressTime) {
			return true;
		}

		return false;
	}

	public void incrementTime(float time) {
		timeSpentPressing += time;
		inputStart = false;
	}
	*/
	}
}