using System;
using System.Collections.Generic;
using Components;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Profiling;

namespace ModularCore {
	public abstract class ModularUI : MonoBehaviour {

		public virtual bool scrolling => false;
		protected bool usingMouse => (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0));

		// Caches the string including the + operator to not make garbage
		private string _profilerName = null;
		private string profilerName {
			get {
				if (_profilerName == null) {
					_profilerName = "ModularUI.update(): " + GetType();
				}
				return _profilerName;
			}
		}

		[NonSerialized] private readonly List<InputVector> ourInputs = new List<InputVector>();
		/// <summary>
		/// The vector we were dragging with last frame because certain things can change the arrangement of the dragging
		/// vectors
		/// </summary>
		private InputVector draggingVector;

		// All children are updated every frame.
		public readonly List<ModularUI> children = new List<ModularUI>();
		// Used for windows. If null all inputs are redirected to it.
		[NonSerialized] public ModularUI blockingUI;

		private RectTransform _rect;
		public virtual RectTransform rect {
			get {
				if (_rect == null) {
					_rect = GetComponent<RectTransform>();
				}

				return _rect;
			}
		}


		public void addElement(ModularUI element) {
			if (element == this) {
				Debug.LogError("Trying to add ourself!");
				return;
			}
			if (children.Count > 20) {
				Debug.LogWarning("There are " + children.Count + " modular items. Check that UI is being removed properly.");
			}
			if (element == null) {
				Debug.LogError("Should not be adding null");
			}
			SceneLocations.updateRoot.queuedForAdding.Add(new MyTupleStruct<ModularUI, ModularUI>(this, element));
		}
		public void removeElement(ModularUI element) {
			SceneLocations.updateRoot.queuedForRemoval.Add(new MyTupleStruct<ModularUI, ModularUI>(this, element));
		}


		// Should only be called after all updateThis() have been executed or we may end up with problems
		public void addElementAtEndOfFrame(ModularUI element) {
			children.Add(element);
		}
		public void removeElementAtEndOfFrame(ModularUI element) {
			if (!children.Remove(element)) {
				Debug.LogError("Could not find modular UI element: " + element + " in " + this + ".");
			}
		}


		public void addBlockingLeafNode(ModularUI blockingNode) {
			SceneLocations.updateRoot.queuedBlockingForAdding.Add(new MyTupleStruct<ModularUI, ModularUI>(this, blockingNode));
		}
		public void addBlockingLeafNodeImmediate(ModularUI blockingNode) {
			if (blockingUI == null) {
				blockingUI = blockingNode;
			} else {
				blockingUI.addBlockingLeafNode(blockingNode);
			}
		}


		public void removeBlockingNode(ModularUI nodeToRemove) {
			SceneLocations.updateRoot.queuedBlockingForRemoval.Add(new MyTupleStruct<ModularUI, ModularUI>(findBlockingUIParent(nodeToRemove), nodeToRemove));
		}
		public void removeBlockingNodeImmediate(ModularUI nodeToRemove) {
			// Fixes a b.ug where timed removing can leave a blockingUI orphaned.
			blockingUI = nodeToRemove.blockingUI;
		}


		private ModularUI findBlockingUIParent(ModularUI child) {
			ModularUI parent = null;

			if (blockingUI == child) {
				return this;
			}

			if (blockingUI != null) {
				parent = blockingUI.findBlockingUIParent(child);
			}

			if (parent != null) {
				return parent;
			}

			for (int i = 0; i < children.Count; i++) {
				parent = children[i].findBlockingUIParent(child);

				if (parent != null) {
					return parent;
				}
			}

			return parent;
		}


		/// <summary>
		/// Returns true iff at least one of our inputs are clicking (in the clicking input)
		/// </summary>
		protected bool clicking(List<InputVector> vectors) {

			for (int i = 0; i < vectors.Count; i++) {
				if (vectors[i].clicking) {
					return true;
				}
			}

			return false;
		}


		/// <summary>
		/// For testing.
		/// </summary>
		public Vector2 screenPositionOfCanvasSpaceObject() {
			Vector3[] positions = new Vector3[]{Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero};
			rect.GetWorldCorners(positions);
			return (positions[0] + positions[1] + positions[2] + positions[3]) * 0.25f;
		}


		/// <summary>
		/// Visible by defined if the center of the rect is not covered by any other rect. Mostly for testing.
		/// </summary>
		public bool visible() {
			return SceneLocations.inputController.findOriginalOwner(screenPositionOfCanvasSpaceObject()).Contains(this);
		}


		/// <summary>
		/// Gets the subset of inputs that are interacting with this.
		/// </summary>
		private void updateOurInputVectors([CanBeNull] List<InputVector> allInputs, List<InputVector> ourInputs) {

			// Reset from last frame
			ourInputs.Clear();

			if (allInputs == null) {
				return;
			}

			// Add all inputs
			for (int i = 0; i < allInputs.Count; i++) {
				if (allInputs[i].inputOwners.Contains(this)) {
					ourInputs.Add(allInputs[i]);
				}
			}
		}


		/// <summary>
		/// Returns if we have at least one input that is not in the mouse up position.
		/// </summary>
		protected bool interactingNonClick(List<InputVector> inputs) {

			for (int i = 0; i < inputs.Count; i++) {
				if (inputs[i].state != InputVector.InputState.End) {
					return true;
				}
			}

			return false;
		}
		/// <summary>
		/// Returns if we have at least one mouse up vector.
		/// </summary>
		protected bool interactingMouseUp(List<InputVector> inputs) {

			for (int i = 0; i < inputs.Count; i++) {
				if (inputs[i].state == InputVector.InputState.End) {
					return true;
				}
			}

			return false;
		}


		/// <summary>
		/// Updates all children recursively. (interactingWithThis() returns true).
		/// </summary>
		public void update(bool atLeastOneInputVector, bool atLeastOneInputVectorClicking) {

			// Cache name (from reflection) and string concatenation name to avoid garbage.
			Profiler.BeginSample(profilerName);

			updateOurInputVectors(InputVector.inputVectorsInUse, ourInputs);
			updateThis(ourInputs, atLeastOneInputVector, atLeastOneInputVectorClicking);

			if (blockingUI != null) {
				blockingUI.update(atLeastOneInputVector, atLeastOneInputVectorClicking);
			}

			// Update children
			for (int i = 0; i < children.Count; i++) {
				children[i].update(atLeastOneInputVector, atLeastOneInputVectorClicking);
			}

			lateUpdate();

			Profiler.EndSample();

		}


		/// <summary>
		/// This should handle ECS rendering.
		/// </summary>
		protected virtual void lateUpdate() {
		}


		/// <summary>
		/// The main ModularUI update loop. It contains all the information we need to know. Some information is kept in a
		/// bit array to save space.
		/// </summary>
		protected abstract void updateThis(List<InputVector> ourInputs, bool atLeastOneInputVector, bool atLeastOneInputVectorClicking);


		/// <summary>
		/// Finds the thing we originally interact with.
		/// </summary>
		public virtual List<ModularUI> findOriginalOwner(Vector2 screenClickLocation) {

			if (!interactingWithThis(screenClickLocation)) {
				return null;
			}

			List<ModularUI> originalOwner = findOriginalOwnerInChildren(screenClickLocation);
			if (originalOwner != null) {
				return originalOwner;
			}

			if (blockingUI == null) {
				return new List<ModularUI>() {this};
			} else {
				return new List<ModularUI>();
			}
		}


		/// <summary>
		/// Checks for original owner in children.
		/// </summary>
		protected virtual List<ModularUI> findOriginalOwnerInChildren(Vector2 screenClickLocation) {

			List<ModularUI> originalOwner;

			if (blockingUI != null) {
				if ((originalOwner = blockingUI.findOriginalOwner(screenClickLocation)) != null) {
					return originalOwner;
				}
			} else {
				for (int i = 0; i < children.Count; i++) {
					if ((originalOwner = children[i].findOriginalOwner(screenClickLocation)) != null) {
						return originalOwner;
					}
				}
			}

			return null;

		}


		public void notifyAllResolutionUpdated() {

			if (blockingUI != null) {
				blockingUI.notifyAllResolutionUpdated();
			}

			for (int i = 0; i < children.Count; i++) {
				children[i].notifyAllResolutionUpdated();
			}

			resolutionUpdated();
		}


		protected virtual void resolutionUpdated() {
		}


		/// <summary>
		/// Notifies all children and then this that a button has been pressed and returns if we took action
		/// on it shortcircuiting the check.
		/// </summary>
		public bool notifyAllButtonUp(KeyCode button) {

			if (blockingUI != null) {
				if (blockingUI.notifyAllButtonUp(button)) {
					return true;
				}
				return false;
			}

			// Notify children and possibly short circuit
			for (int i = 0; i < children.Count; i++) {
				if (children[i].notifyAllButtonUp(button)) {
					return true;
				}
			}

			// Notify us
			return notifyThisButtonUp(button);
		}


		/// <summary>
		/// Notifies us that a button has been pressed and returns if we took action on it
		/// shortcircuiting the check.
		/// </summary>
		protected virtual bool notifyThisButtonUp(KeyCode button) {
			return false;
		}


		protected void notifyAllFocusLost() {

			// Notify children
			for (int i = 0; i < children.Count; i++) {
				children[i].notifyAllFocusLost();
			}

			// Notify us
			focusLost();
		}
		protected virtual void focusLost() {
		}


		/// <summary>
		/// Get the vector we are clicking with
		/// </summary>
		[CanBeNull]
		protected InputVector getDraggingVector(List<InputVector> inputs) {
			if (inputs == null || inputs.Count == 0) {
				return null;
			}

			// Nothing changed
			if (draggingVector != null && inputs.Contains(draggingVector) && draggingVector.state != InputVector.InputState.End) {
				return draggingVector;
			}

			// Find the next dragging vector and return it
			if (draggingVector == null || !inputs.Contains(draggingVector)) {
				for (int i = 0; i < inputs.Count; i++) {
					if (!inputs[i].clicking) {
						draggingVector = inputs[i];
						return draggingVector;
					}
				}
			}

			return null;
		}
		/// <summary>
		/// Get the vector we are clicking with.
		/// </summary>
		[CanBeNull]
		protected InputVector getClickingVector(List<InputVector> inputs) {
			if (inputs == null || inputs.Count == 0) {
				return null;
			}

			// Find the next dragging vector and return it
			for (int i = 0; i < inputs.Count; i++) {
				if (inputs[i].clicking) {
					return inputs[i];
				}
			}

			return null;
		}
		[CanBeNull]
		protected InputVector getLongPressVector(List<InputVector> inputs) {
			if (inputs == null || inputs.Count == 0) {
				return null;
			}

			// Find the next dragging vector and return it
			for (int i = 0; i < inputs.Count; i++) {
				if (inputs[i].longPressing) {
					return inputs[i];
				}
			}

			return null;
		}
		protected InputVector getClickingOrDraggingVector(List<InputVector> inputs) {
			if (inputs == null || inputs.Count == 0) {
				return null;
			}

			// Nothing changed
			if (draggingVector != null && inputs.Contains(draggingVector)) {
				return draggingVector;
			}

			// Find the next dragging vector and return it
			draggingVector = inputs[0];
			return draggingVector;
		}


		/// <summary>
		/// Checks all children recursively for scrolling or animations.
		/// </summary>
		protected virtual bool atLeastOnePaneScrolling() {
			/*
			if (scrolling || SceneLocations.centralizedCoroutines.coroutineRunning(this)) {
				return true;
			}
			*/
			return atLeastOneChildPaneScrolling();
		}
		private bool atLeastOneChildPaneScrolling() {

			if (blockingUI != null) {
				return blockingUI.atLeastOnePaneScrolling();
			}

			for (int i = 0; i < children.Count; i++) {
				if (children[i].atLeastOnePaneScrolling()) {
					return true;
				}
			}

			return false;
		}


		public virtual bool interactingWithThis(Vector2 clickScreenSpace) {
			return clickOverObject(clickScreenSpace, rect);
		}


		/// <summary>
		/// Returns if the click is over the given rect transform.
		/// </summary>
		protected bool clickOverObject(Vector2 clickScreenSpace, RectTransform thingClickingOn) {

			Vector2 topRight = thingClickingOn.transform.TransformPoint(thingClickingOn.rect.max);
			Vector2 bottomLeft = thingClickingOn.transform.TransformPoint(thingClickingOn.rect.min);

			return (clickScreenSpace.x >= bottomLeft.x && clickScreenSpace.x <= topRight.x &&
					clickScreenSpace.y >= bottomLeft.y && clickScreenSpace.y <= topRight.y);
		}


		/// <summary>
		/// Marks this modular UI as no longer dragging (forcing us to refresh the drag data)
		/// </summary>
		public virtual void stopDrag() {
		}
	}
}