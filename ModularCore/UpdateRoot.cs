﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModularCore {

	/// <summary>
	/// Takes Unity's Update() callback and updates all the other UI recursively.
	/// </summary>
	public class UpdateRoot : MonoBehaviour {

		[NonSerialized] private bool executingFrame;
		[SerializeField] public ModularUI rootModularUI;

		// We can not add or remove while iterating over children so just do it at the beginning of each frame
		// (Not end as first frame will look weird)
		public List<MyTupleStruct<ModularUI, ModularUI>> queuedForAdding = new List<MyTupleStruct<ModularUI, ModularUI>>();
		public List<MyTupleStruct<ModularUI, ModularUI>> queuedForRemoval = new List<MyTupleStruct<ModularUI, ModularUI>>();
		public List<MyTupleStruct<ModularUI, ModularUI>> queuedBlockingForAdding = new List<MyTupleStruct<ModularUI, ModularUI>>();
		public List<MyTupleStruct<ModularUI, ModularUI>> queuedBlockingForRemoval = new List<MyTupleStruct<ModularUI, ModularUI>>();

		private void Awake() {
			SceneLocations.updateRoot = this;
		}

		// Update is called once per frame
		private void Update() {

			Tester.update();

			if (executingFrame) {
				if (Debug.isDebugBuild) {
					Debug.LogError("Crash detected");
				} else {
					//Application.Quit();
				}
			}
			executingFrame = true;

			// Must be called at the start of the frame otherwise there will be a one frame off error when loading
			// a map from level loader
			for (int i = 0; i < queuedForAdding.Count; i++) {
				queuedForAdding[i].item1.addElementAtEndOfFrame(queuedForAdding[i].item2);
			}
			queuedForAdding.Clear();
			for (int i = 0; i < queuedForRemoval.Count; i++) {
				queuedForRemoval[i].item1.removeElementAtEndOfFrame(queuedForRemoval[i].item2);
			}
			queuedForRemoval.Clear();
			for (int i = 0; i < queuedBlockingForAdding.Count; i++) {
				queuedBlockingForAdding[i].item1.addBlockingLeafNodeImmediate(queuedBlockingForAdding[i].item2);
			}
			queuedBlockingForAdding.Clear();
			for (int i = 0; i < queuedBlockingForRemoval.Count; i++) {
				queuedBlockingForRemoval[i].item1.removeBlockingNodeImmediate(queuedBlockingForRemoval[i].item2);
			}
			queuedBlockingForRemoval.Clear();

			// Add input vectors
			InputVector.checkAllTouches();
			// If a finger is down it will also activate the mouse. The touchCount check blocks this from happening.
			if (Input.GetMouseButtonDown(0) && Input.touchCount == 0) {
				InputVector.addInputVector(new InputSource(InputType.Mouse), rootModularUI.findOriginalOwner(Input.mousePosition));
			}
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch(i).phase == TouchPhase.Began) {
					InputVector.addInputVector(new InputSource(Input.GetTouch(i).fingerId), rootModularUI.findOriginalOwner(Input.GetTouch(i).position));
				}
			}
			rootModularUI.update(InputVector.inputVectorsInUse.Count > 0, InputVector.atLeastOneInputVectorClicking());
			InputVector.removeTouches();

			executingFrame = false;
		}
	}
}