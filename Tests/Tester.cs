﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using IngameDebugConsole;
using ModularCore;
using UnityEngine;

public static class Tester {

	private static readonly Queue<IEnumerator> tests = new Queue<IEnumerator>();
	private static int totalTests = 0;
	private static bool runningAllTests = false;
	private static int testsPassed = 0;

	// Keeps track if we threw an exception
	private static bool runningATest = false;

	// Use this to simulate touches
	public static Vector2 touchLocation;
	public static InputVector.InputState inputState;

	private static int currentTestNum = 1;

	private static void enqueueAllTests() {
		// Insert your tests like this:
		//tests.Enqueue(TestLander.TestOnlyValidUnitsCanLoadIntoLandersAndLandersMustBeOnBeach());
		//tests.Enqueue(TestLander.TestMax2UnitsToALander());
		//tests.Enqueue(TestUnitActionUI.TestMultiButtonClosesUnitActions());
		//tests.Enqueue(TestFAB.TestFABClicking());
		//tests.Enqueue(TestFAB.TestQuickClicks());
		//tests.Enqueue(TestLevelLoader.TestClicking());
		//tests.Enqueue(TestLander.TestMultiButtons());
	}


	[ConsoleMethod("RunTest", "Runs the given test")]
	public static void initializeGivenTest(int testNum) {
		enqueueAllTests();
		int count = 1;
		while (count < testNum) {
			count++;
			tests.Dequeue();
		}
		IEnumerator test = tests.Dequeue();
		tests.Clear();
		tests.Enqueue(test);
		currentTestNum = testNum;

		totalTests = tests.Count;
		testsPassed = 0;
		Debug.Log("Running tests");
		runningAllTests = true;
		DebugLogManager.instance.Minimize();
	}


	[ConsoleMethod("RunTests", "Runs all tests")]
	public static void initializeTests() {

		enqueueAllTests();

		currentTestNum = 1;
		totalTests = tests.Count;
		testsPassed = 0;
		Debug.Log("Running tests");
		runningAllTests = true;
		DebugLogManager.instance.Minimize();
	}


	/// <summary>
	/// When adding a new test we may only want to run this single test and not all tests.
	/// This method runs only the final test that was added.
	/// </summary>
	[ConsoleMethod("RunLast", "Run the last test added")]
	public static void initializeSingleTest() {

		enqueueAllTests();
		currentTestNum = tests.Count;
		while (tests.Count > 1) {
			tests.Dequeue();
		}

		totalTests = tests.Count;
		testsPassed = 0;
		Debug.Log("Running tests");
		runningAllTests = true;
		DebugLogManager.instance.Minimize();
	}


	public static void update() {

		if (runningATest) {
			tests.Dequeue();

			Debug.LogError("<color=#BB0000>Test " + currentTestNum + " failed. Passes: " + testsPassed + "/" + totalTests + ". Failures: " +
			               (totalTests - testsPassed - tests.Count) + "/" + totalTests + ".</color>");
			currentTestNum++;

			// If test failed make sure to remove mock input
			inputState = InputVector.InputState.Deleted;
		}

		if (tests.Count > 0) {
			runningATest = true;

			if (!tests.Peek().MoveNext()) {
				tests.Dequeue();
				testsPassed++;
				Debug.Log("<color=green>Test " + currentTestNum + " passed. Passes: " + testsPassed + "/" + totalTests + ". Failures: " +
				          (totalTests - testsPassed - tests.Count) + "/" + totalTests + ".</color>");
				currentTestNum++;
			}

		} else if (runningAllTests) {

			if (testsPassed == totalTests) {
				Debug.Log("<color=green>All tests passed!. Total tests: " + totalTests + ".</color>");
			} else {
				Debug.LogError("<color=#BB0000>Finished. Some tests failed. Tests passed " + testsPassed + "/" + totalTests + ". Tests failed: " +
				          (totalTests - testsPassed) + "/" + totalTests + ".</color>");
			}
			runningAllTests = false;
		}

		runningATest = false;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void log(String message) {
		if (!runningAllTests) {
			Debug.Log(message);
		}
	}
}
