﻿using System.Collections.Generic;

public static class Extensions {

	public static void removeAtFast<T>(this List<T> list, int index) {
		list[index] = list[list.Count - 1];
		list.RemoveAt(list.Count - 1);
	}

}
