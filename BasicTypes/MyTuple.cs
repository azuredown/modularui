﻿
/// <summary>
/// I originally created these before C# got Tuples, but they're still useful as C#'s tuples don't have setters for
/// the individual objects.
/// </summary>
public class MyTuple<T1, T2> {
	public T1 item1;
	public T2 item2;

	public MyTuple() {
	}


	public MyTuple(T1 item1, T2 item2) {
		this.item1 = item1;
		this.item2 = item2;
	}
}
public struct MyTupleStruct<T1, T2> {
	public readonly T1 item1;
	public readonly T2 item2;

	public MyTupleStruct(T1 item1, T2 item2) {
		this.item1 = item1;
		this.item2 = item2;
	}
}
public class MyTuple<T1, T2, T3> {
	public T1 item1;
	public T2 item2;
	public T3 item3;

	public MyTuple() {
	}


	public MyTuple(T1 item1, T2 item2, T3 item3) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
	}
}
public struct MyTupleStruct<T1, T2, T3> {
	public readonly T1 item1;
	public readonly T2 item2;
	public readonly T3 item3;

	public MyTupleStruct(T1 item1, T2 item2, T3 item3) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
	}
}
public struct MyTupleStruct<T1, T2, T3, T4> {
	public readonly T1 item1;
	public readonly T2 item2;
	public readonly T3 item3;
	public readonly T4 item4;

	public MyTupleStruct(T1 item1, T2 item2, T3 item3, T4 item4) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
		this.item4 = item4;
	}
}
public class MyTuple<T1, T2, T3, T4> {
	public T1 item1;
	public T2 item2;
	public T3 item3;
	public T4 item4;

	public MyTuple() {
	}


	public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
		this.item4 = item4;
	}
}
