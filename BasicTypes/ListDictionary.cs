﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A dictionary using a list implementation. It maintains constant time access and it does not create garbage.
/// Should be iterated over using indexes rather than foreach.
/// </summary>
public class ListDictionary<Key, Value> {

	private readonly List<MyTuple<Key, Value>> mainList = new List<MyTuple<Key, Value>>();
	private readonly Dictionary<Key, int> itemIndices = new Dictionary<Key, int>();
	private readonly List<Key> reverseIndices = new List<Key>();
	public ICollection<Key> keys => itemIndices.Keys;

	public int count => mainList.Count;

	public bool containsKey(Key key) {
		return itemIndices.ContainsKey(key);
	}


	public void clear() {
		mainList.Clear();
		itemIndices.Clear();
		reverseIndices.Clear();
	}


	public void remove(Key key) {
		removeAt(itemIndices[key]);
	}


	public void removeAt(int i) {

		// Update indices
		itemIndices.Remove(reverseIndices[i]);
		if (mainList.Count > 1 && i != (mainList.Count - 1)) {
			if (!itemIndices.ContainsKey(mainList[mainList.Count - 1].item1)) {
				Debug.LogError("Unable to find item");
			}
			itemIndices[mainList[mainList.Count - 1].item1] = i;
		}

		// Remove items
		mainList.removeAtFast(i);
		reverseIndices.removeAtFast(i);
	}


	public MyTuple<Key, Value> getPair(Key key) {
		return mainList[itemIndices[key]];
	}


	public void add(MyTuple<Key, Value> toAdd) {

		if (containsKey(toAdd.item1)) {
			updateValue(toAdd.item1, toAdd.item2);
		} else {
			mainList.Add(toAdd);
			itemIndices[toAdd.item1] = mainList.Count - 1;
			reverseIndices.Add(toAdd.item1);
		}

	}


	public void add(Key key, Value value) {
		add(new MyTuple<Key, Value>(key, value));
	}


	public MyTuple<Key, Value> this[int i] {
		get { return mainList[i]; }
	}


	public Value atKey(Key key) {
		return mainList[itemIndices[key]].item2;
	}


	private void updateValue(Key key, Value value) {
		mainList[itemIndices[key]].item2 = value;
	}
}
